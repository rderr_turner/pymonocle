#!/usr/bin/python3.4
"""Config settings for the monocle api."""
import os


class PyMonocleConfig:
    """Class for interacting with the monocle api."""

    def __init__(self):
        """Initialize the config based on environment variables."""
        self.env = os.environ
        self.environment = self.env['ENVIRONMENT']
        self.protocol = self.env['PROTOCOL']
        self.host = self.env['MONOCLE_HOST']
        self.activate_uri = self.env['ACTIVATE_URI']
        self.deactivate_uri = self.env['DEACTIVATE_URI']
        self.activate_url = ''.join([self.protocol, self.host,
                                     self.activate_uri])
        self.deactivate_url = ''.join([self.protocol, self.host,
                                       self.deactivate_uri])
